package br.com.socialbooks.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Comentario implements Serializable {

	private static final long serialVersionUID = -5504158376834496652L;

	@Id
	@Column(name = "id_comentario")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idComentario;

	@ManyToOne
	@JoinColumn(name = "id_livro")
	@JsonIgnore
	private Livro livro;

	private String comentario;

	private Date dataComentario;

	public Comentario() {
	}

	public Comentario(Livro livro, String comentario) {
		super();
		this.livro = livro;
		this.comentario = comentario;
	}

	public Long getIdComentario() {
		return idComentario;
	}

	public void setIdComentario(Long idComentario) {
		this.idComentario = idComentario;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Date getDataComentario() {
		return dataComentario;
	}

	public void setDataComentario(Date dataComentario) {
		this.dataComentario = dataComentario;
	}

}
