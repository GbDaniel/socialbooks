package br.com.socialbooks.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Livro implements Serializable {

	private static final long serialVersionUID = -7845062691604475812L;

	@Id
	@Column(name = "id_livro")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idLivro;

	private String tituloLivro;

	private Date publicacao;

	@OneToMany(mappedBy = "livro", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<Comentario> comentarios;

	public Livro() {
	}

	public Livro(Long idLivro) {
		super();
		this.idLivro = idLivro;
	}

	public Livro(String tituloLivro) {
		super();
		this.tituloLivro = tituloLivro;
	}

	public Livro(Long idLivro, String tituloLivro) {
		super();
		this.idLivro = idLivro;
		this.tituloLivro = tituloLivro;
	}

	public Long getIdLivro() {
		return idLivro;
	}

	public void setIdLivro(Long idLivro) {
		this.idLivro = idLivro;
	}

	public String getTituloLivro() {
		return tituloLivro;
	}

	public void setTituloLivro(String tituloLivro) {
		this.tituloLivro = tituloLivro;
	}

	public Date getPublicacao() {
		return publicacao;
	}

	public void setPublicacao(Date publicacao) {
		this.publicacao = publicacao;
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

}
