package br.com.socialbooks.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.socialbooks.model.Livro;
import br.com.socialbooks.service.LivroService;

@RestController
@RequestMapping(value = "/livros")
public class LivroController {

	private LivroService livroService;

	@Autowired
	public LivroController(br.com.socialbooks.service.LivroService livroService) {
		super();
		this.livroService = livroService;
	}

	@RequestMapping(value = "/{idLivro}", method = RequestMethod.GET)
	public ResponseEntity<Livro> buscarLivroPorId(@PathVariable Long idLivro) {
		Livro livro = livroService.buscarLivroPorId(idLivro);
		if (livro == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(livro);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Livro>> listarTodos() {
		return ResponseEntity.ok(livroService.listarTodos());
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Livro> cadastrarLivro(@RequestBody Livro livro) {
		Livro livroSalvo = livroService.cadastrarLivro(livro);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(livroSalvo.getIdLivro())
				.toUri();

		return ResponseEntity.created(uri).body(livroSalvo);
	}

	@RequestMapping(value = "/{idLivro}", method = RequestMethod.PUT)
	public ResponseEntity<Livro> atualizarLivro(@PathVariable Long idLivro, @RequestBody Livro livro) {
		Livro livroAtualizado = livroService.atualizarLivro(livro);
		return ResponseEntity.ok(livroAtualizado);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirLivro(@RequestBody Livro livro) {
		try {
			livroService.excluirLivro(livro);
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}

}
