package br.com.socialbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.socialbooks.model.Livro;

public interface LivroRepository extends JpaRepository<Livro, Long> {

}
