package br.com.socialbooks.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.socialbooks.model.Livro;
import br.com.socialbooks.repository.LivroRepository;

@Service
public class LivroService {

	private LivroRepository livroRepository;

	@Autowired
	public LivroService(LivroRepository livroRepository) {
		super();
		this.livroRepository = livroRepository;
	}

	public List<Livro> listarTodos() {
		return livroRepository.findAll();
	}

	public Livro cadastrarLivro(Livro livro) {
		Livro livroSalvo = livroRepository.save(livro);
		return livroSalvo;
	}

	public Livro buscarLivroPorId(Long idLivro) {
		return livroRepository.getById(idLivro);
	}

	public void excluirLivro(Livro livro) {
		livroRepository.delete(livro);

	}

	public Livro atualizarLivro(Livro livro) {
		return livroRepository.save(livro);
	}
}
