package br.com.socialbooks.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;

import br.com.socialbooks.model.Comentario;
import br.com.socialbooks.model.Livro;
import reactor.core.publisher.Mono;

class LivroControllerTest {

	@BeforeAll
	static void inserirLivro() {
		// configura uma requisição
		Builder builder = WebClient.builder();
		builder.baseUrl("http://localhost:8080");
		builder.filter(ExchangeFilterFunctions.basicAuthentication("danieloliveira", "oliveira"));

		Livro livro = new Livro("Aprendendo Java");
		Livro livro2 = new Livro("Aprendendo Spring boot");

		// prepara o cliente
		WebClient client = builder.build();
		mandaIncluir(livro, client);
		mandaIncluir(livro2, client);
	}

	private static void mandaIncluir(Livro livro, WebClient client) {
		ResponseSpec response = client.method(HttpMethod.POST).uri("/livros").bodyValue(livro).retrieve();

		// recebendo o recurso salvo
		Mono<Livro> monoLivro = response.bodyToMono(Livro.class);
		livro = monoLivro.block();
	}

	@Test
	void testGetListaLivros() {
		// configura uma requisição
		Builder builder = WebClient.builder();
		builder.baseUrl("http://localhost:8080");
		builder.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		builder.filter(ExchangeFilterFunctions.basicAuthentication("danieloliveira", "oliveira"));

		List<Livro> livros = getLivros(builder);

		Assertions.assertTrue(livros.get(0).getTituloLivro().equals("Aprendendo Spring boot")
				|| livros.get(0).getTituloLivro().equals("Aprendendo Java"));

	}

	private List<Livro> getLivros(Builder builder) {
		// prepara o client
		WebClient client = builder.build();
		ResponseSpec response = client.method(HttpMethod.GET).uri("/livros").retrieve();

		/*
		 * recebe o Mono que executa de forma assíncrona. OBS: Para carregar uma lista
		 * de objetos, é preciso usar o ParameterizedTypeReference.
		 */
		Mono<List<Livro>> monoLivro = response.bodyToMono(new ParameterizedTypeReference<List<Livro>>() {
		});

		// como funciona de forma assincrona, usamos o block para forçar ele esperar a
		// resposta.
		List<Livro> livros = monoLivro.block();
		return livros;
	}

	@Test
	void testGetLivro() {
		// configura uma requisição
		Builder builder = WebClient.builder();
		builder.baseUrl("http://localhost:8080");
		builder.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		builder.filter(ExchangeFilterFunctions.basicAuthentication("danieloliveira", "oliveira"));

		// prepara o client
		WebClient client = builder.build();
		ResponseSpec response = client.method(HttpMethod.GET).uri("/livros/2").retrieve();

		// recebe o Mono que executa de forma assíncrona
		Mono<Livro> monoLivro = response.bodyToMono(Livro.class);

		// como funciona de forma assincrona, usamos o block para forçar ele esperar a
		// resposta.
		Livro livro = monoLivro.block();

		Assertions.assertTrue(livro.getTituloLivro().equals("Aprendendo Spring boot"));

	}

	@Test
	void testExcluirLivro() throws Exception {
		// configura uma requisição
		Builder builder = WebClient.builder();
		builder.baseUrl("http://localhost:8080");
		builder.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		builder.filter(ExchangeFilterFunctions.basicAuthentication("danieloliveira", "oliveira"));

		List<Livro> livros = getLivros(builder);

		Livro livro = null;
		if (livros != null && !livros.isEmpty()) {
			livro = livros.remove(0);

			// prepara o client
			WebClient client = builder.build();
			ResponseSpec response = client.method(HttpMethod.DELETE).uri("/livros").bodyValue(livro).retrieve();

			// recebe o Mono que executa de forma assíncrona
			ResponseEntity<Void> responseEntity = response.toBodilessEntity().block();

			Assertions.assertTrue(responseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT));

		} else {
			throw new Exception("Não foi possível excluir um livro");
		}

	}

	@Test
	void testAtualizarLivro() {
		Builder builder = WebClient.builder();
		builder.baseUrl("http://localhost:8080");
		builder.filter(ExchangeFilterFunctions.basicAuthentication("danieloliveira", "oliveira"));

		List<Livro> livros = getLivros(builder);

		Livro livro = null;
		if (livros != null && !livros.isEmpty()) {
			livro = livros.get(0);

			livro.setComentarios(new ArrayList<>());
			Comentario comentario = new Comentario(livro, "Muito bom");
			livro.getComentarios().add(comentario);

			WebClient cliente = builder.build();
			ResponseSpec response = cliente.method(HttpMethod.PUT).uri("/livros/" + livro.getIdLivro()).bodyValue(livro)
					.retrieve();

			livro = response.bodyToMono(Livro.class).block();

			Assertions.assertTrue(livro.getComentarios() != null && livro.getComentarios().get(0) != null);
		}

	}
}
